import React from 'react';
import { useQuery, gql } from '@apollo/client';
import Loading from './Helpers/Loading';
import ErrorComponent from './Helpers/ErrorComponent';
import User from './User/User';
import Posts from './Posts/Posts';

function Main() {
  const GET_USER = gql`
    {
      user(id: 1) {
        name
        email
        phone
        address {
          street
          suite
          city
        }
        company {
          name
        }
        posts {
          data {
            title
          }
        }
      }
    }
  `;

  const { loading, error, data } = useQuery(GET_USER);
  if (loading) return <Loading />;
  if (error) return <ErrorComponent error={error} />;

  return (
    <div className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
      <User
        username={data.user.name}
        address={data.user.address}
        email={data.user.email}
        phone={data.user.phone}
        company={data.user.company.name}
      />
      <Posts username={data.user.name} posts={data.user.posts.data} />
    </div>
  );
}

export default Main;
