import React from 'react';
import UserItem from './UserItem';

function User({ username, address, email, phone, company }) {
  return (
    <div
      id="profile"
      className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0"
    >
      <div className="p-4 md:p-12 text-center lg:text-left">
        <h1 className="text-3xl font-bold pt-8 lg:pt-0">{username}</h1>
        <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
        <UserItem
          field={'Adress'}
          city={address.city}
          street={address.street}
          suite={address.suite}
        />
        <UserItem field={'Email'} data={email} />
        <UserItem field={'Phone'} data={phone} />
        <UserItem field={'Company'} data={company} />
      </div>
    </div>
  );
}

export default User;
