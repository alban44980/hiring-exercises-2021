import React from 'react';

function UserItem({ field, data, city, street, suite }) {
  console.log('DATA FROM USER ITEM  ==> ', data);
  return (
    <>
      {city ? (
        <>
          <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">
            {field}:
          </p>
          <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
            {`${street} ${suite} ${city}`}
          </p>
        </>
      ) : (
        <>
          <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">
            {field}:
          </p>
          <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
            {data}
          </p>
        </>
      )}
    </>
  );
}

export default UserItem;
