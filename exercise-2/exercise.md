### **Brief**

1 - Answer these questions about the design of this [landing](https://logtail.com/)

a) What makes this a good design? What elements? What decisions?

- Call to action well emphasized with the same color ('Integrate in 5min', 'See it in action', 'Learn More', 'Choose')
- The user understand straight away what value the product brings ('Query your logs like you query your database')
- Balance: nice symmetry all along the page, with some pieces are breaking out of that symmetry, which breaks the monotony.
- Great contrast: bright on dark, dark on bright, also essential for accessibility.
- Repetition: white background block, black background block, and so on.
- Good Navigation: in addition to good call to action, the black block doesn't take the entire screen, which allows the user to see the next white block, and indicates that the user can scroll vertically.
- White space: the content is nicely spaced out, which makes it way nicer.
- Simple but good looking animations: mostly on opacity, size and positioning, it is simple but very nice for the user.
- Overall well balanced visual hierarchy: colors, typography, sizes. Everything works well together.
- Quality illustrations that makes the product even more attractive.
- Interactive: slider showing concrete benefits of the product for the user.

b) How do you think was the process of the designers to get to this result?
Here are the steps I belive designer took to make this design:

- List out what they want for their website first: emphazise with the user, thinking about what is the point of the website (Make the product attractive, push the user to interact with the website and try our products, with for instance, click to actions), and list all the features they want for that. The point here is to understand what problem we are solving for the user, and how we solve it.
- From there they started to hierarchise features and call to actions, and started drafting the global structure with a design tool such as Sketch or Figma.
- Once the global structure is established, they started focusing more in detail on the building blocks (we are assuming here that the company already has a color palette and typographic identity): emphazing what is more important by playin with colors, sizes, font sizes, font weight...
- After several reviews, changes and adjustments, they validated this final design.

2 - Using Figma, create a mood board about the landing. Represent these three aspects:

a) Color palette

b) Illustrations style (related in some way to the landing)

c) Typography (find one similar in Google fonts if the original is not free)

### **Important clarifications**

- These 2 points should take you less than an hour.
- The mood board should not be a big design project, it is just to demonstrate you have a well understanding of Figma as a tool. Think of it as a sketching task that should be done in around half an hour, it's shouldn't be pixel perfect.

Link to figma proyect: https://www.figma.com/file/ancOf303HB3bYfYdRNxcTn/Landing-Page?node-id=0%3A1

### Steps

1 ) Fork the repository [https://bitbucket.org/wildaudience/hiring-exercises-2021/src/master/](https://bitbucket.org/wildaudience/hiring-exercises-2021/src/master/) (same as exercise 1)

2. Inside `exercise-2/exercise.md` write down the answers to point 1 and paste link to the figma project of point 2.

3. Commit the changes and push to master. Share the link to the repo and send an invite to the Figma project to `l@wildaudience.com`
